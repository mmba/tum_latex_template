var gulp = require('gulp')
var shell = require('gulp-shell')

gulp.task('default', shell.task(['make', 'make clean']))

// var watcher = gulp.watch(['./main.tex', './bibliography/literature.bib', './pages/', './chapters/', './settings/', './glossary/'])
// var watcher = gulp.watch(['./main.tex', './bibliography/literature.bib', './**/*.tex', './**/*.png', './**/*.csv', './**/*.py'])
var watcher = gulp.watch(['./**.tex', './**/*.bib', './**/*.tex', './**/*.png', './**/*.csv', './**/*.py'])

watcher.on('change', function(e) {
	gulp.start('default');
})
