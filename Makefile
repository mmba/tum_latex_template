# PROJECT=master\'s_thesis
# TEX=pdflatex
# BIBTEX=bibtex
# BUILDTEX=$(TEX) $(PROJECT).tex

# all:
# 	$(BUILDTEX)
# 	$(BIBTEX) $(PROJECT)
# 	$(BUILDTEX)
# 	$(BUILDTEX)

# bibtex:
# 	$(BIBTEX) $(PROJECT)

# #crop:
# #	pdfcrop "gfx/experimentoverview.pdf"


# clean:
# 	rm -f *.dvi *.log *.bak *.aux *.bbl *.idx *.ps *.eps *.toc *.out *~

FILE := main
OUT  := build

pdf:
	# Also see .latexmkrc
	latexmk -outdir=$(OUT) -pdf $(FILE)

clean:
	rm -rf $(filter-out $(OUT)/$(FILE).pdf, $(wildcard $(OUT)/*))

purge:
	rm -rf $(OUT)

.PHONY: latexmk clean purge
